package com.beone.admin.config;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * @title  web status 异常页面配置
 * @Author 覃球球
 * @Version 1.0 on 2018/1/31.
 * @Copyright 长笛龙吟
 */
@Configuration
public class ErrorPageConfig implements EmbeddedServletContainerCustomizer {

    public void customize(ConfigurableEmbeddedServletContainer container) {
        container.addErrorPages(
                new ErrorPage(HttpStatus.BAD_REQUEST, "/error/4O0"),
                new ErrorPage(HttpStatus.UNAUTHORIZED, "/error/4O1"),
                new ErrorPage(HttpStatus.NOT_FOUND, "/error/404")
        );
    }
}
