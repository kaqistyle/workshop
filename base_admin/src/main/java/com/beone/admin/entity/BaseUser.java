package com.beone.admin.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.base.common.BaseModel;

/**
 * @Title 运维数据_系统用户信息表 实体类
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
@TableName("base_user")
public class BaseUser extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 账户ID
            
     */
	@TableId(value="user_id", type= IdType.AUTO)
	private Integer userId;
    /**
     * 账号
     */
	@TableField("user_account")
	private String userAccount;
    /**
     * 密码
     */
	@TableField("user_pwd")
	private String userPwd;
    /**
     * 姓名
     */
	@TableField("user_name")
	private String userName;
    /**
     * 性别 T_user_sex  01-男  02-女
     */
	@TableField("user_sex")
	private String userSex;
    /**
     * 手机号码
     */
	@TableField("user_mobile")
	private String userMobile;
    /**
     * 身份证号
     */
	private String idcard;
    /**
     * 账号状态 S_user_status: 01-正常 09- 禁用   
     */
	@TableField("user_status")
	private String userStatus;
    /**
     * 注册时间
     */
	@TableField("create_time")
	private Integer createTime;
    /**
     * 最后登录时间
     */
	@TableField("last_visit")
	private Integer lastVisit;
    /**
     * 锁定时间
     */
	@TableField("lock_time")
	private Integer lockTime;
    /**
     * 账号密码输入错误次数
     */
	@TableField("error_number")
	private Integer errorNumber;
    /**
     * 是否超级管理员 0-否 1-是
     */
	@TableField("is_admin")
	private Integer isAdmin;

	/** 非数据库映射字段 */
	@TableField(exist = false)
	private String showRoles; //显示用户的角色

	@TableField(exist = false)
	private String roleIds; //用户角色Id

	public String getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}

	public String getShowRoles() {
		return showRoles;
	}

	public void setShowRoles(String showRoles) {
		this.showRoles = showRoles;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserSex() {
		return userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public Integer getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Integer createTime) {
		this.createTime = createTime;
	}

	public Integer getLastVisit() {
		return lastVisit;
	}

	public void setLastVisit(Integer lastVisit) {
		this.lastVisit = lastVisit;
	}

	public Integer getLockTime() {
		return lockTime;
	}

	public void setLockTime(Integer lockTime) {
		this.lockTime = lockTime;
	}

	public Integer getErrorNumber() {
		return errorNumber;
	}

	public void setErrorNumber(Integer errorNumber) {
		this.errorNumber = errorNumber;
	}

	public Integer getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Integer isAdmin) {
		this.isAdmin = isAdmin;
	}

	@Override
	public String toString() {
		return "BaseUser{" +
			", userId=" + userId +
			", userAccount=" + userAccount +
			", userPwd=" + userPwd +
			", userName=" + userName +
			", userSex=" + userSex +
			", userMobile=" + userMobile +
			", idcard=" + idcard +
			", userStatus=" + userStatus +
			", createTime=" + createTime +
			", lastVisit=" + lastVisit +
			", lockTime=" + lockTime +
			", errorNumber=" + errorNumber +
			", isAdmin=" + isAdmin +
			"}";
	}
}
