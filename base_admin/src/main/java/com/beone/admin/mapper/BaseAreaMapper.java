package com.beone.admin.mapper;

import com.beone.admin.entity.BaseArea;
import com.base.SuperMapper;

/**
 *  Mapper 接口
 * @Author 覃球球
 * @Version 1.0 on 2018-10-22
 * @Copyright 贝旺科权
 */
public interface BaseAreaMapper extends SuperMapper<BaseArea> {

}
