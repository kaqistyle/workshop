package com.beone.admin.security.filter;

import com.beone.admin.entity.BasePermission;
import org.springframework.security.core.GrantedAuthority;

/**
 * @Title 自定义基于URL的Spring security权限对象
 *         系统数据库中配置的权限URL
 * @Author 覃忠君 on 2017/12/20.
 * @Copyright © 长笛龙吟
 */
public class PermissionsGrantedAuthority implements GrantedAuthority {

    private final BasePermission permissions;

    public BasePermission getPermissions() {
        return permissions;
    }

    public PermissionsGrantedAuthority(BasePermission permissions) {
        this.permissions = permissions;
    }


    public String getCode() {
        return permissions.getNodeCode();
    }

    /**
     * 根据资源权限code,进行验权处理
     * @return
     */
    public String getAuthority() {
       return getCode();
    }

    public String getUrl() {
        return permissions.getNodeUrl();
    }

    @Override
    public String toString(){
        return getAuthority();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermissionsGrantedAuthority that = (PermissionsGrantedAuthority) o;
        if (getUrl() != null ? !getUrl().equals(that.permissions.getNodeUrl()) : that.permissions.getNodeUrl() != null) return false;
        return getCode() != null ? getCode().equals(that.permissions.getNodeCode()) : that.permissions.getNodeCode() == null;
    }

    @Override
    public int hashCode() {
        int result = getUrl() != null ? getUrl().hashCode() : 0;
        result = 31 * result + (getCode() != null ? getCode().hashCode() : 0);
        return result;
    }
}