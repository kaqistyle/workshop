package com.beone.admin.service;

import com.beone.admin.entity.BaseArea;
import com.base.ISuperService;

/**
 * @Title  服务类
 * @Author 覃球球
 * @Version 1.0 on 2018-10-22
 * @Copyright 贝旺科权
 */
public interface BaseAreaService extends ISuperService<BaseArea> {

}
