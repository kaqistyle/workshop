package com.beone.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.base.common.util.BaseStringUtils;
import com.beone.admin.entity.SysJob;
import com.beone.admin.mapper.SysJobMapper;
import com.beone.admin.service.SysJobService;
import com.base.SuperServiceImpl;
import com.beone.admin.utils.PaginationGatagridTable;
import com.beone.admin.utils.ServiceUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * @Title  系统Job服务实现类
 * @Author 覃球球
 * @Version 1.0 on 2018-10-25
 * @Copyright 贝旺科权
 */
@Service
public class SysJobServiceImpl extends SuperServiceImpl<SysJobMapper, SysJob> implements SysJobService {

    /**
     * 分页显示后台任务列表
     * @param job
     * @param currPage  当前页码
     * @param pageSize  每页显示记录数
     * @return
     */
    public PaginationGatagridTable getSysJobPagination(SysJob job, int currPage, int pageSize){
        EntityWrapper<SysJob> ew = new EntityWrapper<SysJob>();
        if(StringUtils.isNotBlank(job.getJobName())){
            ew.eq("job_name", BaseStringUtils.filterSpecSpecialValue(job.getJobName()));
        }

        if(StringUtils.isNotBlank(job.getJobDesc())){
            ew.eq("job_desc", BaseStringUtils.filterSpecSpecialValue(job.getJobDesc()));
        }
        ew.orderBy("create_date", false);

        Page<SysJob> page = new Page<SysJob>(currPage,pageSize);
        List<SysJob> rows = baseMapper.selectPage(page, ew);
        return ServiceUtils.createGatagridTableJson(page,rows);
    }

    /**
     * 保存任务
     * @param sysJob
     * @return
     */
    public boolean saveJob(SysJob sysJob){
        boolean result = false;
        if(StringUtils.isBlank(sysJob.getId())){ // add
            sysJob.setId(BaseStringUtils.getUuid());
            sysJob.setCreateDate(new Date());
            sysJob.setStatus(0); //不启动
        }else{ //update
            sysJob.setUpdateDate(new Date());
        }
        result = super.insertOrUpdate(sysJob);
        return result;
    }
}
