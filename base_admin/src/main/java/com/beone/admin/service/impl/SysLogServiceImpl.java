package com.beone.admin.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.beone.admin.entity.SysLog;
import com.beone.admin.mapper.SysLogMapper;
import com.beone.admin.service.SysLogService;
import com.base.SuperServiceImpl;
import com.beone.admin.utils.PaginationGatagridTable;
import com.beone.admin.utils.ServiceUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @Title 日志管理 服务实现类
 * @Author 覃球球
 * @Version 1.0 on 2018-10-22
 * @Copyright 贝旺科权
 */
@Service
public class SysLogServiceImpl extends SuperServiceImpl<SysLogMapper, SysLog> implements SysLogService {
    /**
     * 分页显示后台日志列表
     * @param startTime
     * @param endTime
     * @param currPage  当前页码
     * @param pageSize  每页显示记录数
     * @return
     */
    public PaginationGatagridTable getSysLogPagination(String startTime, String endTime, int currPage, int pageSize){
        EntityWrapper<SysLog> ew = new EntityWrapper<SysLog>();
        if(StringUtils.isNotBlank(startTime)){
            ew.ge("DATE_FORMAT(create_time, '%Y-%m-%d')", startTime);
        }

        if(StringUtils.isNotBlank(endTime)){
            ew.le("DATE_FORMAT(create_time, '%Y-%m-%d')", endTime);
        }
        ew.orderBy("create_time", false);

        Page<SysLog> page = new Page<SysLog>(currPage,pageSize);
        List<SysLog> rows = baseMapper.selectPage(page, ew);
        return ServiceUtils.createGatagridTableJson(page,rows);
    }
}
